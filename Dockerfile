FROM golang:1.17.5-alpine3.14 AS build
WORKDIR /app
COPY . /app
RUN env GOOS=linux GOARCH=amd64 go build

FROM alpine:3.14.3 AS pack
RUN addgroup -S app && adduser -S app -G app
USER app
WORKDIR /home/app
COPY --from=build /app/app /home/app
CMD [ "./app" ]
